#include <Wire.h>;
#include <LiquidCrystal_I2C.h>;
LiquidCrystal_I2C lcd(0x27,16,2);

void scroll(int line, char str[]) {
  static int i, j, k;
  i = strlen(str);
  for (j = 16; j >= 0; j--) {
    lcd.setCursor(0, line);
    for (k = 0; k <= 15; k++) {
      lcd.print(" "); 
    }
    lcd.setCursor(j, line);
    lcd.print(str1);
    delay(250);
  }
}

void setup() {
  lcd.init();
  lcd.backlight();
  lcd.print("Test Hello");
  delay(500);
}

void loop() {
  lcd.clear();
  scroll(0, "Hello World");
}
